#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <stdlib.h>
#include <string.h>


using namespace std;

//Funciones Para Accesorios
//Inicio Accesorios
void valint(int* p,int a)
{
	*p=a;
}
//Fin Accesorios

//Funciones Para Cartas
//Inicio Cartas
typedef enum clase{Picas,Treboles,Diamantes,Corazones} clase_carta;
typedef enum valor{A,DOS,TRES,CUATRO,CINCO,SEIS,SIETE,OCHO,NUEVE,J,Q,K} valor_carta;

struct Carta
{
	clase_carta claseC;
	valor_carta valorC;
};

void obtenervalor(Carta* refcar,bool* camb)
{
	int val;
	switch((*refcar).valorC)
	{
		case A:
		{
			if (*camb)
			{
				valint(&val,11);
			}
			else
			{
				valint(&val,1);
			}
			break;
		}
		case DOS:
		{
			valint(&val,2);
			break;
		}
		case TRES:
		{
			valint(&val,3);
		}
		case CUATRO:
		{
			valint(&val,4);
		}
		case CINCO:
		{
			valint(&val,5);
		}
		case SEIS:
		{
			valint(&val,6);
		}
		case SIETE:
		{
			valint(&val,7);
		}
		case OCHO:
		{
			valint(&val,8);
		}
		case NUEVE:
		{
			valint(&val,9);
		}
		case J:
		case Q:
		case K:
		{
			valint(&val,10);
		}
	}
}

//Fin Cartas

//Funciones Para Baraja
//Inicio Baraja
struct Mano
{
	int num;
	Carta *papel;
};

Mano bcontrincante;
Mano player;

void pedircarta()
{
		
}

void baraja()
{
		
}

void contrincante()
{
	
}

void iniciar()
{
	
}
//Fin Baraja

//Funciones Para Menu
//Inicio Menu
void menuprinci()
{
	int _temp;
	cout << "---------------" <<endl;
	cout << "Bienvenido a Juego de Cartas" <<endl;
	cout << "---------------" <<endl;
	cout << "¿Que desea hacer?"<<endl;
	cout << "1. Juego Nuevo" <<endl;
	cout << "2. Salir" <<endl;
	cout << "---------------" <<endl;
	scanf("%i",&_temp);
	if (_temp==1)
	{
		//Inicio del Juego
	}
	else if (_temp==2)
	{
		//Salida
	}
	else
	{
		//Se repite la opcion menu
		menuprinci();
	}
}
//Fin Menu


int main(int argc, char **argv)
{
	menuprinci();
	return 0;
}
